<?php

/**
 * Implementation of hook_views_data().
 */
function similarity_views_data() {
  $data = array();
  $all = similarity_all_similarities(TRUE);
  foreach($all as $sim_obj) {
    $data = array_merge($data, $sim_obj->views_data());
  }
  return $data;
}
