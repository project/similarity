<?php
/**
 * This function defines all the types of similarities
 */
function similarity_similarity_definitions() {
  return array(
    'node_search' => array(
      'title' => t('Node Search similarity'),
      'description' => t('Use search index to draw similarities'),
      'class' => 'similarity_search_similarity',
      'requires' => array(
        'search'
      )
    ),
    'node_title' => array(
      'title' => t('Node title'),
      'description' => t('Use nodes titles to draw similarities'),
      'class' => 'similarity_title'
    ),
    'node_terms' => array(
      'title' => t('Node Terms'),
      'description' => t('Use a nodes taxonomy terms to draw similarities'),
      'class' => 'similarity_taxonomy',
    ),
  );
}

/**
 * create a similarity object
 *
 * @param sid
 * sid of the similarity object
 *
 * @param type
 * the type of the similarity
 */
function similarity_create($sid, $type = NULL) {
  if (!$type) {
    $type = db_result(db_query("SELECT type FROM {similarity_objects} WHERE sid = %d", $sid));
  }
  $types = similarity_similarity_definitions();
  $definition = $types[$type];
  $all_installed = TRUE;
  if (isset($definition['requires'])) {
    foreach($definition['requires'] as $module) {
      if (!module_exists($module)) {
        $all_installed = FALSE;
      }
    }
  }
  if ($all_installed) {
    // TODO: test this
    if ($definition['file']) {
      include_once './' . $definition['file'];
    }
    // create it
    return new $definition['class']($sid);
  }
}

/**
 * load up all the similarity objects
 *
 * @return
 * array of similarity objects pulled from the db directly
 */
function similarity_all_similarities($construct = FALSE) {
  $result = db_query("SELECT * FROM {similarity_objects}");
  $all = array();
  while($sim = db_fetch_object($result)) {
    if (!$construct) {
      $all[] = $sim;
    }
    else {
      $all[] = similarity_create($sim->sid, $sim->type);
    }
  }
  
  return $all;
}

/**
 * Abstract klass defining the basics of the similarity class
 */
abstract class similarity {
  // internal id used as a primary key
  public $sid = 0;
  
  // the type of similarity. shall be implemented in overriding classes
  public $type;
  
  // Human readable title of the similarity class
  public $title;
  
  // machine readable name
  public $machine_name;
  
  // serialized data
  public $data;
  
  // calculate the similarities for the type
  // TODO: use the calculation_limits function to get the limits
  abstract public function calculate();
  
  // generate the second step of the form
  // defining whatever is needed by the class
  abstract public function options_form();
  
  // table schema
  abstract protected function schema();
  
  // views data
  abstract public function views_data();
  
  /**
   * set the limits for calculation
   *
   * @return
   * array of limits for processing
   */
  public function calculation_limits() {
    // TODO: implement this. See notifications/messaging framework
    return array('number' => 100);
  }
  
  /**
   * basic constructor
   * This constructor maintains the similarity_objects table
   *
   * @param sid
   * sid can be null allows for the creation of a blank similarity object
   * and then add in the fields (minus sid) by hand, and a save()
   */
  public function __construct($sid = NULL) {
    if (!empty($sid)) {
      $db_object = db_fetch_object(db_query("SELECT * FROM {similarity_objects} WHERE sid = %d", $sid));
      foreach($db_object as $field => $value) {
        $this->$field = $value;
      }
      
      // grab the stuff and unserialize the data field
      drupal_unpack($this, 'data');
    }
  }
  
  /**
   * save the similarity object to the database
   */
  public function save() {
    // pass the proper update param to drupal_write_record
    $update = ($this->sid) ? 'sid' : NULL;
    
    $sucess = drupal_write_record('similarity_objects', $this, $update);
    if (!$sucess) {
      drupal_set_message(t('Failed to save the similarity object to the database'), 'error');
    }
    
    if ($sucess == SAVED_NEW) {
      // for semantics, we now have an sid
      $this->sid = db_last_insert_id('similarity_objects', 'sid');
      
      // create the objects table
      $ret = array();
      $schema = $this->schema();
      db_create_table(&$ret, $this->machine_name . '_similarity', $schema);
    }
  }
    
  /**
   * save the serialized verision of the second step of creation
   */
  public function serialize($form_values) {    
    // remove the things we know we don't want saved
    unset($form_values['similarity_object']);
    unset($form_values['op']);
    unset($form_values['submit']);
    unset($form_values['form_build_id']);
    unset($form_values['form_token']);
    unset($form_values['form_id']);
    
    $this->data = serialize($form_values);
  }
}

/**
 * node similarity based on the search index
 */
class similarity_search_similarity extends similarity_node {
  
  // keyed array of nid -> words
  // acts as a static cache
  private $word_cache = array();
  
  
  /**
   * Implementation of the calculate function
   * cosine similarity of the search index
   */
  public function calculate() {
    // TODO: shutdown functions and globals
    
    // get the possible nids based on limits
    // TODO: implement with better limits
    $possible_nids = array();
    foreach($this->types as $node_type) {
      if (empty($type_string)) {
        $type_string = "type = '%s'";
      }
      else {
        $type_string .= " OR type = '%s'";
      }
    }
    $type_string = "(" . $type_string . ")";
    $db_nids = db_query("SELECT nid FROM {node} WHERE " . $type_string, $this->types);
    while($nid_obj = db_fetch_object($db_nids)) {
      $possible_nids[] = $nid_obj->nid;
    }
    
    // emulate node_update_index()
    $last = variable_get($this->machine_name . '_cron_last', 0);
    // only delete when we have run this before
    $delete_first = ($last != 0);
    $last_nid = variable_get($this->machine_name . '_cron_last_nid', 0);
    $limit = $this->node_limit; // very basic right now
    $args = array($last, $last_nid, $last, $last);
    $args = array_merge($args, $this->types);
    $result = db_query_range('SELECT GREATEST(IF(c.last_comment_timestamp IS NULL, 0, c.last_comment_timestamp), n.changed) as last_change, n.nid FROM {node} n LEFT JOIN {node_comment_statistics} c ON n.nid = c.nid WHERE n.status = 1 AND ((GREATEST(n.changed, c.last_comment_timestamp) = %d AND n.nid > %d) OR (n.changed > %d OR c.last_comment_timestamp > %d)) AND ' . $type_string . ' ORDER BY GREATEST(n.changed, c.last_comment_timestamp) ASC, n.nid ASC', $args, 0, $limit);
    
    $value_string = '';
    $sim_cache = array();
    // cosine of the search index for the node against all other nodes
    // TODO: check limit on each passing changed_obj
    while ($changed_obj = db_fetch_object($result)) {
      $current_words = $this->get_words($changed_obj->nid);
      $last_changed = $changed_obj->last_change;
      $last_nid = $changed_obj->nid;
      foreach($possible_nids as $other_nid) {
        if ($other_nid != $changed_obj->nid) {
          $others_words = $this->get_words($other_nid);
          
          $similarity = similarity_cosine($current_words, $others_words);
           if ($similarity != 0) {
            if ($other_nid > $changed_obj->nid) {
              $id1 = $other_nid;
              $id2 = $changed_obj->nid;
            }
            else {
              $id1 = $changed_obj->nid;
              $id2 = $other_nid;
            }
            
            if (!isset($sim_cache[$id1][$id2])) {
              $value_string .= "($id1, $id2, $similarity), ";
              $value_string .= "($id2, $id1, $similarity), ";
              $delete_string .= "(nid1 = $id1 AND nid2 = $id2) OR ";
              $delete_string .= "(nid1 = $id2 AND nid2 = $id1) OR ";
              $sim_cache[$id1][$id2] = $similarity;
            }
          }
        }
      }
    }
    if (!empty($value_string)) {
      // wipe dataset first
      if ($delete_first) {
        db_query("DELETE FROM {" . $this->machine_name . "_similarity} WHERE " . substr($delete_string, 0, -4));
      }
      // then insert
      db_query("INSERT INTO {" . $this->machine_name . "_similarity} VALUES " . substr($value_string, 0, -2));
      variable_set($this->machine_name . '_cron_last', $last_changed);
      variable_set($this->machine_name . '_cron_last_nid', $last_nid);
    }
  }
  
  /**
   * fetches the words for a given nid
   * prevents hitting the db a crazy amount, only once per nid
   */
  protected function get_words($nid) {    
    if (!isset($this->word_cache[$nid])) {
      // now we have to query for the words
      $db_words = db_query("SELECT word, score FROM {search_index} WHERE sid = %d AND type = 'node'", $nid);
      while($word = db_fetch_object($db_words)) {
        $this->word_cache[$nid][$word->word] = $word->score;
      }
    }
    
    return $this->word_cache[$nid];
  }
}



abstract class similarity_node extends similarity {
  protected function schema() {
    $schema = array(
      'fields' => array(
        'nid1' => array(
          'type' => 'int',
          'size' => 'normal',
          'not null' => TRUE,
        ),
        'nid2' => array(
          'type' => 'int',
          'size' => 'normal',
          'not null' => TRUE,
        ),
        'score' => array(
          'type' => 'float',
          'not null' => TRUE,
        ),
      ),
      'primary key' => array('nid1', 'nid2'),
    );
    
    return $schema;
  }
  
  /**
   * Implementation of the option form function
   *
   * @return
   * structured array (Drupal form)
   */
  public function options_form() {
    $node_types = node_get_types('names');
    $form['node_limit'] = array(
      '#type' => 'select',
      '#title' => t('Node Throttle'),
      '#options' => drupal_map_assoc(range(50, 500, 50)),
      '#description' => t('Select the number of node similarities to calculate per cron run'),
      '#default_value' => isset($this->node_limit) ? $this->node_limit : 100,
    );
    $form['types'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Node types'),
      '#description' => t('Select the node types that should be used to calculate similarities'),
      '#options' => $node_types,
      '#default_value' => isset($this->types) ? $this->types : array(),
    );
    return $form;
  }
  
  /**
   * override of serialize function
   */
  public function serialize(&$form_values) {
    foreach($form_values['types'] as $key => $name) {
      if (!empty($name)) {
        $form_values['types'][] = $key;
      }
      unset($form_values['types'][$key]);
    }
    parent::serialize($form_values);
  }
  
  /**
   * returns the structure array for its tables
   */
  function views_data() {
    $table_name = $this->machine_name . '_similarity';
    
    $data[$table_name]['table']['group'] = t('Similarity');
    $data[$table_name]['table']['join'] = array(
      'node' => array(
        'left_field' => 'nid',
        'field' => 'nid1',
        'type' => 'INNER',
      ),
    );
    $data[$table_name]['nid2'] = array(
      'title' => t('@name Similary NID', array('@name' => $this->title)),
      'field' => array(
        'handler' => 'views_handler_field_numeric',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_node_nid',
      ),
      'help' => t('Node id of the similar node'),
    );
    
    $data[$table_name]['score'] = array(
      'title' => t('@name Similarity score', array('@name' => $this->title)),
      'field' => array(
        'handler' => 'views_handler_field_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'help' => t('Similarity score of the second node'),
    );

    return $data;
  }
}

/**
 * node title similarity. uses the nodes title to determine how
 * similar each node is
 */
class similarity_title extends similarity_node {
  
  /**
   * Implementation of the option form function
   *
   * @return
   * structured array (Drupal form)
   */
  public function options_form() {
    $form = parent::options_form();
    $form['gram_size'] = array(
      '#type' => 'select',
      '#title' => t('Size of the n-gram'),
      '#description' => t('This object will build an index of titles based on n-sized characters. This options configures how large n is'),
      '#options' => drupal_map_assoc(array(1, 2, 3, 4, 5, 6)),
      '#default_value' => $this->gram_size,
    );
    return $form;
  }
  
  /**
   * Implementation of the calculate function
   * cosine similarity of the search index
   */
  public function calculate() {
    // TODO: shutdown functions and globals
    
    // get the possible nids based on limits
    // TODO: implement with better limits
    $possible_nids = array();
    $type_string = '';
    $args = array();
    $sim_cache = array();
    foreach($this->types as $node_type) {
      if (empty($type_string)) {
        $type_string = "type = '%s'";
      }
      else {
        $type_string .= " OR type = '%s'";
      }
    }
    $type_string = "(" . $type_string . ")";
    $db_nids = db_query("SELECT nid, title FROM {node} WHERE " . $type_string, $this->types);
    while($nid_obj = db_fetch_object($db_nids)) {
      $possible_nids[$nid_obj->nid] = $nid_obj->title;
    }
    
    // set limits
    $last = variable_get($this->machine_name . '_cron_last', 0);
    $limit = $this->node_limit; // very basic right now
    $delete_first = TRUE;
    $args[] = $last;
    $args = array_merge($args, $this->types);
    $result = db_query_range('SELECT nid, created, changed FROM {node} WHERE changed > %d AND ' . $type_string . ' ORDER BY changed ASC', $args, 0, $limit);
    $last_changed = 0;
    
    $value_string = '';
    while ($changed_obj = db_fetch_object($result)) {
      $current_words = $this->ngram($possible_nids[$changed_obj->nid]);
      $last_changed = $changed_obj->changed;
      
      // we have at least one changed node. so we need to delete first
      if ($changed_obj->changed != $changed_obj->created) {
        $delete_first = TRUE;
      }
      foreach($possible_nids as $other_nid => $title) {
        if ($other_nid != $changed_obj->nid) {
          $others_words = $this->ngram($title);
          
          // TODO: to keep storage down only store unique pairs
          // use spatial index and INSERT IGNORE
          $similarity = similarity_cosine($current_words, $others_words);
          if ($similarity != 0) {
            if ($other_nid > $changed_obj->nid) {
              $id1 = $other_nid;
              $id2 = $changed_obj->nid;
            }
            else {
              $id1 = $changed_obj->nid;
              $id2 = $other_nid;
            }
            
             if (!isset($sim_cache[$id1][$id2])) {
              $value_string .= "($id1, $id2, $similarity), ";
              $value_string .= "($id2, $id1, $similarity), ";
              $delete_string .= "(nid1 = $id1 AND nid2 = $id2) OR ";
              $delete_string .= "(nid1 = $id2 AND nid2 = $id1) OR ";
              $sim_cache[$id1][$id2] = $similarity;
            }
          }
        }
      }
    }
    if (!empty($value_string)) {
      // wipe dataset first
      if ($delete_first) {
        db_query("DELETE FROM {" . $this->machine_name . "_similarity} WHERE " . substr($delete_string, 0, -4));
      }
      // then insert
      db_query("INSERT INTO {" . $this->machine_name . "_similarity} VALUES " . substr($value_string, 0, -2));
      variable_set($this->machine_name . '_cron_last', $last_changed);
    }
  }
  
  /**
   * returns an array of grams generated from the text
   *
   * @param text
   * the plain text to be 'gramed'
   *
   * @return
   * array of grams generated from the text as keys to
   * a score of 1. ie. all grams are equal
   */
  protected function ngram($text) {
    // get the n-gram
    $gram_size = $this->gram_size;
    // elminate stop words
    // elminate all spaces
    $text = $this->stop_words($text);
    $words = array();
   
    // step through each of the text
    // and add it modified text as space deliminated
    $words = array();
    for($start = 0; $start + $gram_size <= strlen($text); $start++) {
      $words[substr($text, $start, $gram_size)] = 1;
    }
    // words is an array of grams
    return $words;
  }
  
  /**
   * elminates stop words and all spaces
   * 
   * @param text
   * the space delminted non-html text
   *
   * @return
   * string of words without spaces, and without the stop words defined within
   * function
   */
  protected function stop_words($text) {
    $stop_words = array(
         'a',
      'able',
      'about',
      'above',
      'abroad',
      'according',
      'accordingly',
      'across',
      'actually',
      'adj',
      'after',
      'afterwards',
      'again',
      'against',
      'ago',
      'ahead',
      'aint',
      'all',
      'allow',
      'allows',
      'almost',
      'alone',
      'along',
      'alongside',
      'already',
      'also',
      'although',
      'always',
      'am',
      'amid',
      'amidst',
      'among',
      'amongst',
      'an',
      'and',
      'another',
      'any',
      'anybody',
      'anyhow',
      'anyone',
      'anything',
      'anyway',
      'anyways',
      'anywhere',
      'apart',
      'appear',
      'appreciate',
      'appropriate',
      'are',
      'arent',
      'around',
      'as',
      'as',
      'aside',
      'ask',
      'asking',
      'associated',
      'at',
      'available',
      'away',
      'awfully',
      'b',
      'back',
      'backward',
      'backwards',
      'be',
      'became',
      'because',
      'become',
      'becomes',
      'becoming',
      'been',
      'before',
      'beforehand',
      'begin',
      'behind',
      'being',
      'believe',
      'below',
      'beside',
      'besides',
      'best',
      'better',
      'between',
      'beyond',
      'both',
      'brief',
      'but',
      'by',
      'c',
      'came',
      'can',
      'cannot',
      'cant',
      'cant',
      'caption',
      'cause',
      'causes',
      'certain',
      'certainly',
      'changes',
      'clearly',
      'cmon',
      'co',
      'co.',
      'com',
      'come',
      'comes',
      'concerning',
      'consequently',
      'consider',
      'considering',
      'contain',
      'containing',
      'contains',
      'corresponding',
      'could',
      'couldnt',
      'course',
      'cs',
      'currently',
      'd',
      'dare',
      'darent',
      'definitely',
      'described',
      'despite',
      'did',
      'didnt',
      'different',
      'directly',
      'do',
      'does',
      'doesnt',
      'doing',
      'done',
      'dont',
      'down',
      'downwards',
      'during',
      'e',
      'each',
      'edu',
      'eg',
      'eight',
      'eighty',
      'either',
      'else',
      'elsewhere',
      'end',
      'ending',
      'enough',
      'entirely',
      'especially',
      'et',
      'etc',
      'even',
      'ever',
      'evermore',
      'every',
      'everybody',
      'everyone',
      'everything',
      'everywhere',
      'ex',
      'exactly',
      'example',
      'except',
      'f',
      'fairly',
      'far',
      'farther',
      'few',
      'fewer',
      'fifth',
      'first',
      'five',
      'followed',
      'following',
      'follows',
      'for',
      'forever',
      'former',
      'formerly',
      'forth',
      'forward',
      'found',
      'four',
      'from',
      'further',
      'furthermore',
      'g',
      'get',
      'gets',
      'getting',
      'given',
      'gives',
      'go',
      'goes',
      'going',
      'gone',
      'got',
      'gotten',
      'greetings',
      'h',
      'had',
      'hadnt',
      'half',
      'happens',
      'hardly',
      'has',
      'hasnt',
      'have',
      'havent',
      'having',
      'he',
      'hed',
      'hell',
      'hello',
      'help',
      'hence',
      'her',
      'here',
      'hereafter',
      'hereby',
      'herein',
      'heres',
      'hereupon',
      'hers',
      'herself',
      'hes',
      'hi',
      'him',
      'himself',
      'his',
      'hither',
      'hopefully',
      'how',
      'howbeit',
      'however',
      'hundred',
      'i',
      'id',
      'ie',
      'if',
      'ignored',
      'ill',
      'im',
      'immediate',
      'in',
      'inasmuch',
      'inc',
      'inc.',
      'indeed',
      'indicate',
      'indicated',
      'indicates',
      'inner',
      'inside',
      'insofar',
      'instead',
      'into',
      'inward',
      'is',
      'isnt',
      'it',
      'itd',
      'itll',
      'its',
      'its',
      'itself',
      'ive',
      'j',
      'just',
      'k',
      'keep',
      'keeps',
      'kept',
      'know',
      'known',
      'knows',
      'l',
      'last',
      'lately',
      'later',
      'latter',
      'latterly',
      'least',
      'less',
      'lest',
      'let',
      'lets',
      'like',
      'liked',
      'likely',
      'likewise',
      'little',
      'look',
      'looking',
      'looks',
      'low',
      'lower',
      'ltd',
      'm',
      'made',
      'mainly',
      'make',
      'makes',
      'many',
      'may',
      'maybe',
      'maynt',
      'me',
      'mean',
      'meantime',
      'meanwhile',
      'merely',
      'might',
      'mightnt',
      'mine',
      'minus',
      'miss',
      'more',
      'moreover',
      'most',
      'mostly',
      'mr',
      'mrs',
      'much',
      'must',
      'mustnt',
      'my',
      'myself',
      'n',
      'name',
      'namely',
      'nd',
      'near',
      'nearly',
      'necessary',
      'need',
      'neednt',
      'needs',
      'neither',
      'never',
      'neverf',
      'neverless',
      'nevertheless',
      'new',
      'next',
      'nine',
      'ninety',
      'no',
      'nobody',
      'non',
      'none',
      'nonetheless',
      'noone',
      'no-one',
      'nor',
      'normally',
      'not',
      'nothing',
      'notwithstanding',
      'novel',
      'now',
      'nowhere',
      'o',
      'obviously',
      'of',
      'off',
      'often',
      'oh',
      'ok',
      'okay',
      'old',
      'on',
      'once',
      'one',
      'ones',
      'ones',
      'only',
      'onto',
      'opposite',
      'or',
      'other',
      'others',
      'otherwise',
      'ought',
      'oughtnt',
      'our',
      'ours',
      'ourselves',
      'out',
      'outside',
      'over',
      'overall',
      'own',
      'p',
      'particular',
      'particularly',
      'past',
      'per',
      'perhaps',
      'placed',
      'please',
      'plus',
      'possible',
      'presumably',
      'probably',
      'provided',
      'provides',
      'q',
      'que',
      'quite',
      'qv',
      'r',
      'rather',
      'rd',
      're',
      'really',
      'reasonably',
      'recent',
      'recently',
      'regarding',
      'regardless',
      'regards',
      'relatively',
      'respectively',
      'right',
      'round',
      's',
      'said',
      'same',
      'saw',
      'say',
      'saying',
      'says',
      'second',
      'secondly',
      'see',
      'seeing',
      'seem',
      'seemed',
      'seeming',
      'seems',
      'seen',
      'self',
      'selves',
      'sensible',
      'sent',
      'serious',
      'seriously',
      'seven',
      'several',
      'shall',
      'shant',
      'she',
      'shed',
      'shell',
      'shes',
      'should',
      'shouldnt',
      'since',
      'six',
      'so',
      'some',
      'somebody',
      'someday',
      'somehow',
      'someone',
      'something',
      'sometime',
      'sometimes',
      'somewhat',
      'somewhere',
      'soon',
      'sorry',
      'specified',
      'specify',
      'specifying',
      'still',
      'sub',
      'such',
      'sup',
      'sure',
      't',
      'take',
      'taken',
      'taking',
      'tell',
      'tends',
      'th',
      'than',
      'thank',
      'thanks',
      'thanx',
      'that',
      'thatll',
      'thats',
      'thats',
      'thatve',
      'the',
      'their',
      'theirs',
      'them',
      'themselves',
      'then',
      'thence',
      'there',
      'thereafter',
      'thereby',
      'thered',
      'therefore',
      'therein',
      'therell',
      'therere',
      'theres',
      'theres',
      'thereupon',
      'thereve',
      'these',
      'they',
      'theyd',
      'theyll',
      'theyre',
      'theyve',
      'thing',
      'things',
      'think',
      'third',
      'thirty',
      'this',
      'thorough',
      'thoroughly',
      'those',
      'though',
      'three',
      'through',
      'throughout',
      'thru',
      'thus',
      'till',
      'to',
      'together',
      'too',
      'took',
      'toward',
      'towards',
      'tried',
      'tries',
      'truly',
      'try',
      'trying',
      'ts',
      'twice',
      'two',
      'u',
      'un',
      'under',
      'underneath',
      'undoing',
      'unfortunately',
      'unless',
      'unlike',
      'unlikely',
      'until',
      'unto',
      'up',
      'upon',
      'upwards',
      'us',
      'use',
      'used',
      'useful',
      'uses',
      'using',
      'usually',
      'v',
      'value',
      'various',
      'versus',
      'very',
      'via',
      'viz',
      'vs',
      'w',
      'want',
      'wants',
      'was',
      'wasnt',
      'way',
      'we',
      'wed',
      'welcome',
      'well',
      'well',
      'went',
      'were',
      'were',
      'werent',
      'weve',
      'what',
      'whatever',
      'whatll',
      'whats',
      'whatve',
      'when',
      'whence',
      'whenever',
      'where',
      'whereafter',
      'whereas',
      'whereby',
      'wherein',
      'wheres',
      'whereupon',
      'wherever',
      'whether',
      'which',
      'whichever',
      'while',
      'whilst',
      'whither',
      'who',
      'whod',
      'whoever',
      'whole',
      'wholl',
      'whom',
      'whomever',
      'whos',
      'whose',
      'why',
      'will',
      'willing',
      'wish',
      'with',
      'within',
      'without',
      'wonder',
      'wont',
      'would',
      'wouldnt',
      'x',
      'y',
      'yes',
      'yet',
      'you',
      'youd',
      'youll',
      'your',
      'youre',
      'yours',
      'yourself',
      'yourselves',
      'youve',
      'z',
      'zero',
    );
   
    // remove apostrophes and split  into words
    $terms = preg_split('/([^a-zA-Z]+)/', str_replace("'", '', $text), -1,  PREG_SPLIT_NO_EMPTY  );
   
    // check for stop words
    for($i = 0; $i < count($terms); $i++) {
      if (in_array(trim($terms[$i]), $stop_words)) {
        unset($terms[$i]);
      }
    }
   
    // combine and remove the spaces
    return implode('', $terms);
  }
}


class similarity_taxonomy extends similarity_node {
  
  /**
   * Override the base calculate function
   */
  function calculate() {
    // get the possible nids based on limits
    // TODO: implement with better limits
    $possible_nids = array();
    $type_string = '';
    $args = array();
    $sim_cache = array();
    foreach($this->types as $node_type) {
      if (empty($type_string)) {
        $type_string = "n.type = '%s'";
      }
      else {
        $type_string .= " OR n.type = '%s'";
      }
    }
    $type_string = "(" . $type_string . ")";
    $db_nids = db_query("SELECT n.nid, r.vid FROM {node} n JOIN {node_revisions} r ON n.vid = r.vid WHERE " . $type_string, $this->types);
    while($nid_obj = db_fetch_object($db_nids)) {
      $terms = taxonomy_node_get_terms($nid_obj);
      foreach($terms as $term_data_row) {
        // TODO: can we use term_data->weight?
        $possible_nids[$nid_obj->nid][strtolower($term_data_row->name)] = 1;
      }
    }
    
    // set limits
    $last = variable_get($this->machine_name . '_cron_last', 0);
    $limit = $this->node_limit; // very basic right now
    $delete_first = FALSE;
    $args[] = $last;
    $args = array_merge($args, $this->types);
    $result = db_query_range('SELECT n.nid, n.changed FROM {node} n WHERE n.changed > %d AND ' . $type_string . ' ORDER BY changed ASC', $args, 0, $limit);
    $last_changed = 0;
    while ($changed_obj = db_fetch_object($result)) {
      $current_terms = $possible_nids[$changed_obj->nid];
      $last_changed = $changed_obj->changed;
      
      // we have at least one changed node. so we need to delete first
      if ($changed_obj->changed != $changed_obj->created) {
        $delete_first = TRUE;
      }
      if (!empty($current_terms)) {
        foreach($possible_nids as $other_nid => $other_terms) {
          if ($other_nid != $changed_obj->nid) {
            $similarity = similarity_cosine($current_terms, $other_terms);          
            
            if ($similarity != 0) {
              if ($other_nid > $changed_obj->nid) {
                $id1 = $other_nid;
                $id2 = $changed_obj->nid;
              }
              else {
                $id1 = $changed_obj->nid;
                $id2 = $other_nid;
              }
              
              if (!isset($sim_cache[$id1][$id2])) {
                $value_string .= "($id1, $id2, $similarity), ";
                $value_string .= "($id2, $id1, $similarity), ";
                $delete_string .= "(nid1 = $id1 AND nid2 = $id2) OR ";
                $delete_string .= "(nid1 = $id2 AND nid2 = $id1) OR ";
                $sim_cache[$id1][$id2] = $similarity;
              }
            }
          }
        }
      }
    }
    if (!empty($value_string)) {
      // wipe dataset first
      if ($delete_first) {
        db_query("DELETE FROM {" . $this->machine_name . "_similarity} WHERE " . substr($delete_string, 0, -4));
      }
      // then insert
      db_query("INSERT INTO {" . $this->machine_name . "_similarity} VALUES " . substr($value_string, 0, -2));
      variable_set($this->machine_name . '_cron_last', $last_changed);
    }
  }
}

// util functions
/**
 * calculates teh cosine between objects a and b
 *
 * @param $a
 * array of revelancy scores keyed by word
 * ie 'wizard' => 7.33333
 *
 * @param $b
 * array of revelancy scores keyed by word
 * ie 'wizard' => 7.33333
 * 
 * @see get_words
 */
function similarity_cosine($a, $b) {
  $dot_product = 0;
  $a_total = 0;
  $b_total = 0;
  
  $total_words = array_merge($a, $b);
  
  // only care about the key $word
  foreach($total_words as $word => $junk_score) {
    if (isset($a[$word]) && isset($b[$word])) {
      $dot_product += $a[$word] * $b[$word];
    }
    
    if (isset($a[$word])) {
      $a_total += pow($a[$word], 2);
    }
    
    if (isset($b[$word])) {
      $b_total += pow($b[$word], 2);
    }
  }
  
  // if nothing worked
  if ($a_total == 0 || $b_total == 0 ) {
    return 0;
  }
  
  return ($dot_product / sqrt($a_total * $b_total));
}
